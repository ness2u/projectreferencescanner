﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication74
{
  public class GistGenerator
  {
    public static string GenerateGist(Dictionary<string, Node<SimpleAssembly>> refs)
    {
      var sb = new StringBuilder();
      sb.AppendLine("[source,cypher]");
      sb.AppendLine("----");

      foreach (var node in refs.Values)
      {
        // create nodes
        var line = string.Format("CREATE ({1}:{0}{{name:'{2}', display_name:'{3}'}})",
            node.Value.Type,
            node.Value.Name.Replace(".", "_").ToLower(),
            node.Value.Name,
            node.Value.DisplayName);
        sb.AppendLine(line);
      }
      foreach (var node in refs.Values)
      {                  
        foreach (var dep in node.Dependencies)
        {
          // link nodes
          var line = string.Format("CREATE ({0})-[:{2}]->({1})",
              node.Value.Name.Replace(".", "_").ToLower(),
              dep.Item1.Value.Name.Replace(".", "_").ToLower(),
              dep.Item2.Type);
          sb.AppendLine(line);
        }
      }

      sb.AppendLine("----");
      sb.AppendLine("//graph");

      var graph = sb.ToString();
      return graph;
    }
  }
}
