﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication74
{
  public class Node<T>
  {
    public Node(T value)
    {
      Dependencies = new List<Tuple<Node<T>, SimpleDependency>>();
      Value = value;
    }

    public T Value { get; set; }
    public List<Tuple<Node<T>, SimpleDependency>> Dependencies { get; set; }
  }
}