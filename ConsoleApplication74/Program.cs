﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApplication74
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var providers = ProjectScanner.ScanProjects(
              @"C:\Development\Starbucks\Platform\Main",
              @"C:\Development\API");

            var refs = BuildProviderGraph(providers);

            var graph = GistGenerator.GenerateGist(refs);
            File.WriteAllText(@"C:\Users\jblahut\Documents\DomainArchitecture\gist.txt", graph);

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(providers, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("providers.json", json);
        }

        private static Dictionary<string, Node<SimpleAssembly>> BuildProviderGraph(List<SimpleAssembly> providers)
        {
            var refs = new Dictionary<string, Node<SimpleAssembly>>();
            foreach (var p in providers)
            {
                refs.Add(p.Name, new Node<SimpleAssembly>(p));
            }

            foreach (var p in providers)
            {
                var node = refs[p.Name];

                foreach (var dep in p.Dependencies)
                {
                    Node<SimpleAssembly> n;
                    if (refs.TryGetValue(dep.Assembly, out n))
                    {
                        node.Dependencies.Add(new Tuple<Node<SimpleAssembly>, SimpleDependency>(n, dep));
                    }
                }
            }
            return refs;
        }
    }
}
