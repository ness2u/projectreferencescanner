﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace ConsoleApplication74
{
    public class ProjectScanner
    {
        public static List<SimpleAssembly> ScanProjects(params string[] paths)
        {
            var files = GetFileList(paths);

            var seen = new HashSet<string>();
            var list = new List<SimpleAssembly>();
            var dupes = new List<SimpleAssembly>();
            foreach (var file in files)
            {
                using (var stream = File.OpenRead(file.FullName))
                {
                    var elements = XElement.Load(XmlReader.Create(stream));

                    var fileName = Path.GetFileNameWithoutExtension(file.Name);
                    var assemblyName = elements.Descendants()
                        .Where(r => r.Name.LocalName == "AssemblyName")
                        .Select(r => r.Value)
                        .FirstOrDefault() ?? fileName;

                    var outputType = elements.Descendants()
                        .Where(r => r.Name.LocalName == "OutputType")
                        .Select(r => r.Value)
                        .First();

                    var refs = GetAssemblyRefs(elements);
                    var projectRefs = GetProjectRefs(elements, file);


                    var type = "provider";
                    if (string.Equals("exe", outputType, StringComparison.OrdinalIgnoreCase))
                    {
                        type = "exe";
                    }
                    else if (assemblyName.IndexOf("dal", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        type = "dal";
                    }
                    else if (assemblyName.IndexOf("common", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        type = "common";
                    }
                    else if (assemblyName.IndexOf("provider", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        type = "provider";
                    }
                    else if (assemblyName.IndexOf("api", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        type = "webapi";
                    }
                    else if (assemblyName.IndexOf("queueservice", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        type = "queueservice";
                    }
                    else if (assemblyName.IndexOf("services", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        type = "services2b";
                    }


                    var provider = new SimpleAssembly()
                    {
                        Name = assemblyName,
                        DisplayName = MakeDisplayName(assemblyName),
                        Type = type,
                        Dependencies = refs.Concat(projectRefs).ToList(),
                        Path = file.FullName
                    };

                    if (seen.Add(assemblyName))
                    {
                        list.Add(provider);
                    }
                    else
                    {
                        Console.WriteLine("DUPLICATE ASSEMBLY NAME!!!! " + assemblyName);
                        dupes.Add(provider);
                    }
                }
            }
            return list;
        }

        private static IEnumerable<SimpleDependency> GetProjectRefs(XContainer elements, FileSystemInfo file)
        {
            var projectRefs = elements.Descendants()
              .Where(r => r.Name.LocalName == "ProjectReference")
              .Select(r => r.Attribute("Include").Value)
              .Select(r => Path.GetFullPath(Path.Combine(Path.GetDirectoryName(file.FullName) ?? "", r)))
              .Select(GetAssemblyName)
              .Where(r => r != null)
              .Select(r => new SimpleDependency()
              {
                  Assembly = r,
                  Type = "project"
              })
              .ToList();
            return projectRefs;
        }

        private static IEnumerable<SimpleDependency> GetAssemblyRefs(XContainer elements)
        {
            var refs = elements.Descendants()
              .Where(r => r.Name.LocalName == "Reference")
              .Select(r =>
                  new {
                      Include = r.Attribute("Include").Value,
                      HintPath = r.Elements().FirstOrDefault(e => e.Name.LocalName == "HintPath")
                  })
              .Select(r =>
              {
                  var type = "assembly";
                  if (r.HintPath != null)
                  {
                      if (r.HintPath.Value.Contains("..\\packages\\"))
                      {
                          type = "nuget";
                      }
                      else if (r.HintPath.Value.Contains("..\\ThirdParty\\"))
                      {
                          type = "GROSS_ThirdParty_path_assembly";
                      }
                      else if (r.HintPath.Value.Contains("\\bin\\"))
                      {
                          type = "bin_path_assembly";
                      }
                      else
                      {
                          type = "unknown_path_assembly";
                      }
                  }
                  return new SimpleDependency()
                  {
                      Assembly = CleanDep(r.Include),
                      Type = type
                  };
              })
              .ToList();
            return refs;
        }

        private static IEnumerable<FileInfo> GetFileList(IEnumerable<string> paths)
        {
            var pattern = new Regex(@"\\Main", RegexOptions.IgnoreCase);
            var files = paths.SelectMany(path => (new DirectoryInfo(path)).GetFiles("*.csproj", SearchOption.AllDirectories)
              .Where(f => f.FullName.IndexOf("test", StringComparison.OrdinalIgnoreCase) < 0))
              .Where(f => pattern.IsMatch(f.FullName))
              .ToList();
            return files;
        }

        private static string MakeDisplayName(string assemblyName)
        {
            assemblyName = assemblyName
                .Replace("Starbucks.", string.Empty)
                .Replace("OpenApi.", string.Empty);

            return assemblyName;
        }

        private static string CleanDep(string dep)
        {
            if (dep.Contains(","))
            {
                dep = dep.Substring(0, dep.IndexOf(",", StringComparison.OrdinalIgnoreCase));
            }
            return dep;
        }

        private static readonly Dictionary<string, string> AssemblyCache = new Dictionary<string, string>();
        private static string GetAssemblyName(string path)
        {
            string name;
            if (AssemblyCache.TryGetValue(path, out name)) { return name; }

            try
            {
                using (var stream = File.OpenRead(path))
                {
                    var elements = XElement.Load(XmlReader.Create(stream));
                    var assemblyName = elements.Descendants()
                                .Where(r => r.Name.LocalName == "AssemblyName")
                                .Select(r => r.Value)
                                .FirstOrDefault();

                    AssemblyCache[path] = assemblyName;

                    return assemblyName;
                }
            }
            catch
            {
                Console.WriteLine("Cannot get assembly name of project: {0}", path);
                return null;
            }
        }
    }
}
