using System.Collections.Generic;

namespace ConsoleApplication74
{
  public class SimpleAssembly
  {
    public string Name { get; set; }
    public string Type { get; set; }
    public List<SimpleDependency> Dependencies { get; set; }
    public string Path { get; set; }
    public string DisplayName { get; set; }

    public override string ToString()
    {
      return Name;
    }

  }
}