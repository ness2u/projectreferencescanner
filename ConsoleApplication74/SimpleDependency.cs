﻿namespace ConsoleApplication74
{
  public class SimpleDependency
  {
    public string Assembly { get; set; }
    public string Type { get; set; }

    public override string ToString()
    {
      return $"{Type}, {Assembly}";
    }
  }
}